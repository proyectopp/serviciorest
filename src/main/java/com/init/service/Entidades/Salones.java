package com.init.service.Entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Salon") 
public class Salones {
	@Id
	@Column(name="salon")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_salon;
	
	@Column(name="nombre", nullable=false , length = 30)
	private String nombre;
	
	@Column(name="bloque", nullable = false, length = 30)
	private String bloque;
	
	@Column(name="sede",nullable = false, length = 30)
	private String sede;

	public long getId_salon() {
		return id_salon;
	}

	public void setId_salon(long id_salon) {
		this.id_salon = id_salon;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getBloque() {
		return bloque;
	}

	public void setBloque(String bloque) {
		this.bloque = bloque;
	}

	public String getSede() {
		return sede;
	}

	public void setSede(String sede) {
		this.sede = sede;
	}
	

}
