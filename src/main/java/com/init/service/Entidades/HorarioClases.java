package com.init.service.Entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="horario")
public class HorarioClases {
	@Id
	@Column(name="id_horario")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id_materia;
	
	@Column(name="nombre", nullable=false , length = 30)
	private String nombre;
	
	@Column(name="grupo", nullable = false, length = 30)
	private String grupo;
	
	@Column(name="sede",nullable = false, length = 30)
	private String sede;
	
	@Column(name="bloque",nullable = false, length = 30)
	private String bloque;

	@Column(name="salon", nullable = false , length = 30)
	private String salon;
	
	@Column(name="dia", nullable = false, length = 30)
	private String dia;
	
	
	public long getId_materia() {
		return id_materia;
	}
	public void setId_materia(long id_materia) {
		this.id_materia = id_materia;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getSede() {
		return sede;
	}
	public void setSede(String sede) {
		this.sede = sede;
	}
	public String getBloque() {
		return bloque;
	}
	public void setBloque(String bloque) {
		this.bloque = bloque;
	}
	public String getSalon() {
		return salon;
	}
	public void setSalon(String salon) {
		this.salon = salon;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	
}
