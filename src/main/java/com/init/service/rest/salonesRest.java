package com.init.service.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.init.service.Entidades.HorarioClases;
import com.init.service.Entidades.Salones;
import com.init.service.dao.salonDAO;
import com.init.service.dao.serviciosDAO;

@RestController
@RequestMapping("salones")

public class salonesRest {
	@Autowired
	private salonDAO salonDAO;

	@GetMapping
	public ResponseEntity<List<Salones>> getSalones() {
		List<Salones> salones = salonDAO.findAll();
		return ResponseEntity.ok(salones);
	}

	@RequestMapping(value = "{id_salon}") // horario/{hcId}-> /horario/1 //busca por ID
	public ResponseEntity<Salones> getSalonesById(@PathVariable("id_salon") long id_salon) {

		Optional<Salones> opcional = salonDAO.findById(id_salon);
		if (opcional.isPresent()) {
			return ResponseEntity.ok(opcional.get());

		} else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@DeleteMapping(value = "{id_salon}") // horarios (DELETE)
	public ResponseEntity<Void> deleteSalom(@PathVariable("id_salon") long id_salon) {
		salonDAO.deleteById(id_salon);
		return ResponseEntity.ok(null);
	}

}
