package com.init.service.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Optional;

import com.init.service.Entidades.HorarioClases;
import com.init.service.dao.serviciosDAO;

@RestController
@RequestMapping("horario")

public class productRest {

	@Autowired
	private serviciosDAO DAOO;

	@GetMapping
	public ResponseEntity<List<HorarioClases>> getHorariosClases() {
		List<HorarioClases> hc = DAOO.findAll();
		return ResponseEntity.ok(hc);
	}

	@RequestMapping(value = "{horaclasesID}") // horario/{hcId}-> /horario/1 //busca por ID
	public ResponseEntity<HorarioClases> getHorariosClasesById(@PathVariable("horaclasesID") long horaclase) {

		Optional<HorarioClases> opcional = DAOO.findById(horaclase);
		if (opcional.isPresent()) {
			return ResponseEntity.ok(opcional.get());

		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@PostMapping // horarios (POST)
	public ResponseEntity<HorarioClases> createHoraClase(@RequestBody HorarioClases horaclases) {
		HorarioClases newHoraclases = DAOO.save(horaclases);
		return ResponseEntity.ok(newHoraclases);
	}

	@DeleteMapping(value = "{horaID}") // horarios (DELETE)
	public ResponseEntity<Void> deleteHoraClase(@PathVariable("horaID") long horaID) {
		DAOO.deleteById(horaID);
		return ResponseEntity.ok(null);
	}

	@PutMapping //modifica  mediante el metodo PUT ruta
	public ResponseEntity<HorarioClases> updateHoraClase(@RequestBody HorarioClases horarioclases) {
		Optional<HorarioClases> opcional = DAOO.findById(horarioclases.getId_materia());
		if (opcional.isPresent()) {
			HorarioClases updaHorarioClases = opcional.get();
			updaHorarioClases.setNombre(horarioclases.getNombre());
			DAOO.save(updaHorarioClases);
			return ResponseEntity.ok(horarioclases);

		} else {
			return ResponseEntity.noContent().build();
		}
	}

}
